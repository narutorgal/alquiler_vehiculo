
package com.alquiler.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.JOptionPane;

public class VehiculoDao {

    PreparedStatement ps;
    ResultSet rs;
    Connection con;
    Conexion conectar = new Conexion();
    Vehiculo vh = new Vehiculo();

    public List listar() {
        List<Vehiculo> datos = new ArrayList<>();
        String sql = "SELECT auto.id, auto.placa,marca.sublista,linea.sublista,  "
                + "CASE WHEN auto.estado= 1 THEN 'Disponible' "
                + "WHEN auto.estado= 2 THEN 'Alquilado' "
                + "Else 'Eliminado' END as estado_vehiculo "
                + "FROM auto "
                + "LEFT JOIN sublista linea ON auto.linea = linea.id "
                + "LEFT JOIN sublista marca ON auto.marca = marca.id "
                + "WHERE auto.estado != 0;";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Vehiculo p = new Vehiculo();
                p.setId(rs.getInt(1));
                p.setPlaca(rs.getString(2));
                p.setMarca(rs.getString(3));
                p.setLinea(rs.getString(4));
                p.setEstado(rs.getString(5));
                datos.add(p);
            }
        } catch (Exception e) {
            System.out.println(e);
            System.out.println("error en consulta " + sql);
        }
        return datos;
    }

    public int agregar(Vehiculo per) {
        int r = 0;
        String sql = "insert into auto(id,placa,marca,linea)values(?,?,?,?)";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, per.getId());
            ps.setString(2, per.getPlaca());
            ps.setString(3, per.getMarca());
            ps.setString(4, per.getLinea());
            r = ps.executeUpdate();
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
        }
        return r;
    }

    public int Actualizar(Vehiculo per) {
        int r = 0;
        String sql = "update auto set placa=?,marca=?,linea=? where Id=?";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, per.getPlaca());
            ps.setString(2, per.getMarca());
            ps.setString(3, per.getLinea());
            ps.setInt(4, per.getId());
            r = ps.executeUpdate();
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            System.out.println(sql);
            System.out.println(e);
        }
        return r;
    }

    public int Estado(int id) {
        int r = 0;
        String sql = "update auto set estado = 2 where Id=" + id;
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            r = ps.executeUpdate();
        } catch (Exception e) {
        }
        return r;
    }

    public int Delete(int id) {
        int r = 0;
        String sql = "update auto set estado = 0 where Id=" + id;
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            r = ps.executeUpdate();
        } catch (Exception e) {
        }
        return r;
    }

    public Vector<Vehiculo> getVehiculo() {
        String sql = "SELECT * "
                + "FROM auto "
                + "WHERE auto.estado = 1;";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            Vector<Vehiculo> data = new Vector<Vehiculo>();
            if (rs.next()) {
                do {
                    Vehiculo p = new Vehiculo();
                    p.setId(rs.getInt(1));
                    p.setPlaca(rs.getString(2));
                    p.setMarca(rs.getString(3));
                    p.setLinea(rs.getString(4));
                    p.setEstado(rs.getString(5));
                    data.addElement(p);
                } while (rs.next());
                return data;

            } else {
                return null;
            }
        } catch (Exception ex) {
            System.out.print(ex);
            System.out.print(sql);
            return null;
        }
    }

    public Vector<Vehiculo> getVehiculo1(String placa) {
        String sql = "SELECT * "
                + "FROM auto "
                + "WHERE auto.estado != 0 and auto.placa LIKE '%" + placa + "%'";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            Vector<Vehiculo> data = new Vector<Vehiculo>();
            if (rs.next()) {
                do {
                    Vehiculo p = new Vehiculo();
                    p.setId(rs.getInt(1));
                    p.setPlaca(rs.getString(2));
                    p.setMarca(rs.getString(3));
                    p.setLinea(rs.getString(4));
                    p.setEstado(rs.getString(5));
                    data.addElement(p);
                } while (rs.next());
                return data;

            } else {
                return null;
            }
        } catch (Exception ex) {
            System.out.print(ex);
            System.out.print(sql);
            return null;
        }
    }
}
