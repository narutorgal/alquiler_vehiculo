
package com.alquiler.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.JOptionPane;

public class SublistaDao {

    PreparedStatement ps;
    ResultSet rs;
    Connection con;
    Conexion conectar = new Conexion();
    Sublista s = new Sublista();

    public int agregar(Sublista per) {
        int r = 0;
        String sql = "insert into sublista(id,id_lista,sublista)values(?,?,?)";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, per.getId());
            ps.setString(2, per.getId_lista());
            ps.setString(3, per.getSublista());
            r = ps.executeUpdate();
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return r;
    }

    public int Actualizar(Sublista per) {
        int r = 0;
        String sql = "update sublista set id_lista=?,sublista=? where Id=?";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, per.getId_lista());
            ps.setString(2, per.getSublista());
            ps.setInt(3, per.getId());
            r = ps.executeUpdate();
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return r;
    }

    public int Delete(int id) {
        int r = 0;
        String sql = "update sublista set estado = 0 where Id=" + id;
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            r = ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
        return r;
    }

    public Vector<Listas> getTipos() {
        String sql = "SELECT * "
                + "FROM listas "
                + "WHERE listas.estado != 0;";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            Vector<Listas> data = new Vector<Listas>();
            if (rs.next()) {
                do {
                    Listas p = new Listas();
                    p.setId(rs.getInt(1));
                    p.setEstado(rs.getString(2));
                    data.addElement(p);
                } while (rs.next());
                return data;

            } else {
                return null;
            }
        } catch (Exception ex) {
            System.out.print(ex);
            return null;
        }
    }

    public Vector<Sublista> getMarcas1(String id) {
        String sql = "SELECT * "
                + "FROM sublista "
                + "WHERE sublista.id_lista = 1 and sublista.estado != 0 and sublista.sublista LIKE '%" + id + "%'";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            Vector<Sublista> data = new Vector<Sublista>();
            if (rs.next()) {
                do {
                    Sublista p = new Sublista();
                    p.setId(rs.getInt(1));
                    p.setId_lista(rs.getString(2));
                    p.setSublista(rs.getString(3));
                    p.setEstado(rs.getString(4));
                    data.addElement(p);
                } while (rs.next());
                return data;

            } else {
                return null;
            }
        } catch (Exception ex) {
            System.out.print(ex);
            return null;
        }
    }

    public Vector<Sublista> getLineas1(String id) {
        String sql = "SELECT * "
                + "FROM sublista "
                + "WHERE sublista.id_lista = 2 and sublista.estado != 0 and sublista.sublista ='" + id + "'";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            Vector<Sublista> data = new Vector<Sublista>();
            if (rs.next()) {
                do {
                    Sublista p = new Sublista();
                    p.setId(rs.getInt(1));
                    p.setId_lista(rs.getString(2));
                    p.setSublista(rs.getString(3));
                    p.setEstado(rs.getString(4));
                    data.addElement(p);
                } while (rs.next());
                return data;
            } else {
                return null;
            }
        } catch (Exception ex) {
            System.out.print(ex);
            System.out.print(sql);
            return null;
        }
    }

    public Vector<Sublista> getTipo1(String id) {
        String sql = "SELECT * "
                + "FROM sublista "
                + "WHERE sublista.id_lista = 3 and sublista.estado != 0 and sublista.sublista LIKE '%" + id + "%'";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            Vector<Sublista> data = new Vector<Sublista>();
            if (rs.next()) {
                do {
                    Sublista p = new Sublista();
                    p.setId(rs.getInt(1));
                    p.setId_lista(rs.getString(2));
                    p.setSublista(rs.getString(3));
                    p.setEstado(rs.getString(4));
                    data.addElement(p);
                } while (rs.next());
                return data;

            } else {
                return null;
            }
        } catch (Exception ex) {
            System.out.print(ex);
            return null;
        }
    }

    public Vector<Sublista> ListaDeObjetosParaComboBox() {
        String sql = "SELECT * "
                + "FROM sublista "
                + "WHERE sublista.id_lista=1 and sublista.estado != 0;";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            Vector<Sublista> data = new Vector<Sublista>();
            if (rs.next()) {
                do {
                    Sublista p = new Sublista();
                    p.setId(rs.getInt(1));
                    p.setId_lista(rs.getString(2));
                    p.setSublista(rs.getString(3));
                    p.setEstado(rs.getString(4));
                    data.addElement(p);
                } while (rs.next());
                return data;

            } else {
                return null;
            }
        } catch (Exception ex) {
            System.out.print(ex);
            return null;
        }
    }

    public Vector<Sublista> ListaDeObjetosParaComboBox2() {
        String sql = "SELECT * "
                + "FROM sublista "
                + "WHERE sublista.id_lista=2 and sublista.estado != 0;";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            Vector<Sublista> data = new Vector<Sublista>();
            if (rs.next()) {
                do {
                    Sublista p = new Sublista();
                    p.setId(rs.getInt(1));
                    p.setId_lista(rs.getString(2));
                    p.setSublista(rs.getString(3));
                    p.setEstado(rs.getString(4));
                    data.addElement(p);
                } while (rs.next());
                return data;

            } else {
                return null;
            }
        } catch (Exception ex) {
            System.out.print(ex);
            return null;
        }
    }

    public Vector<Sublista> ListaDeObjetosParaComboBox3() {
        String sql = "SELECT * "
                + "FROM sublista "
                + "WHERE sublista.id_lista=3 and sublista.estado != 0;";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            Vector<Sublista> data = new Vector<Sublista>();
            if (rs.next()) {
                do {
                    Sublista p = new Sublista();
                    p.setId(rs.getInt(1));
                    p.setId_lista(rs.getString(2));
                    p.setSublista(rs.getString(3));
                    p.setEstado(rs.getString(4));
                    data.addElement(p);
                } while (rs.next());
                return data;

            } else {
                return null;
            }
        } catch (Exception ex) {
            System.out.print(ex);
            return null;
        }
    }
}
