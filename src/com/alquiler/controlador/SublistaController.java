package com.alquiler.controlador;

import com.alquiler.modelo.User;
import com.alquiler.modelo.UserDao;
import com.alquiler.vista.vistausuario;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import com.alquiler.modelo.Conexion;
import com.alquiler.modelo.Listas;
import com.alquiler.modelo.ListasDao;
import com.alquiler.modelo.Sublista;
import com.alquiler.modelo.SublistaDao;
import com.alquiler.vista.vistaconfig;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

public class SublistaController implements ActionListener {

    Conexion cc = new Conexion();
    Connection cn = cc.getConnection();
    SublistaDao dao = new SublistaDao();
    ListasDao daol = new ListasDao();
    Sublista p = new Sublista();
    vistaconfig vista = new vistaconfig();
    DefaultTableModel modelo = new DefaultTableModel();

    public SublistaController(vistaconfig v) {
        this.vista = v;
        this.vista.btnListar.addActionListener(this);
        this.vista.btnAgregar.addActionListener(this);
        this.vista.btnEditar.addActionListener(this);
        this.vista.btnDelete.addActionListener(this);
        this.vista.btnActualizar.addActionListener(this);
        this.vista.btnNuevo.addActionListener(this);
        this.vista.Btn_reset.addActionListener(this);
        this.vista.Btn_buscar.addActionListener(this);
        vista.List_tipop.addActionListener(this);
        bloquear();
        getLista();
    }

    public final void getLista() {
        Vector<Listas> oListaObjetoParaComboBox = daol.getTipos();
        if (oListaObjetoParaComboBox.size() > 0) {
            for (int i = 0; i < oListaObjetoParaComboBox.size(); i = i + 1) {
                Listas contar = oListaObjetoParaComboBox.get(i);
                int id = contar.getId();
                String nombre = contar.getLista();
                Listas oItem = new Listas(id, nombre);
                vista.List_tipop.addItem(oItem);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No hay marcas registrados en la Base de datos", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);
        }
    }
    
    public void getSelect(String rol) {
        if (rol != null) {
            Vector<Listas> oListaObjetoParaComboBox = daol.getRoles1(rol);
            if (oListaObjetoParaComboBox.size() > 0) {
                for (int i = 0; i < oListaObjetoParaComboBox.size(); i++) {
                    Listas contar = oListaObjetoParaComboBox.get(i);
                    Listas oItem1 = new Listas(contar.getId(), contar.getLista());
                    vista.List_tipop.removeAllItems();
                    vista.List_tipop.addItem(oItem1);
                    vista.List_tipop.setSelectedIndex(i++);
                    getLista();
                }
            } else {
                JOptionPane.showMessageDialog(null, "No hay marcas registrados en la Base de datos", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            System.out.println("no envia nada");
        }
    }

    void bloquear() {
        vista.btnListar.setEnabled(true);
        vista.btnNuevo.setEnabled(true);
        vista.Btn_reset.setEnabled(false);
        vista.btnEditar.setEnabled(false);
        vista.btnDelete.setEnabled(false);
        vista.btnActualizar.setEnabled(false);
        vista.btnAgregar.setEnabled(false);
    }

    void desbloquear1() {
        vista.List_tipop.setEnabled(true);
        vista.Txt_nombre.setEnabled(true);
        vista.btnListar.setEnabled(true);
        vista.btnNuevo.setEnabled(false);
        vista.Btn_reset.setEnabled(false);
        vista.btnEditar.setEnabled(false);
        vista.btnDelete.setEnabled(false);
        vista.btnActualizar.setEnabled(false);
        vista.btnAgregar.setEnabled(true);
    }

    void desbloquear2() {
        vista.btnListar.setEnabled(false);
        vista.btnNuevo.setEnabled(true);
        vista.Btn_reset.setEnabled(false);
        vista.btnEditar.setEnabled(true);
        vista.btnDelete.setEnabled(true);
        vista.btnActualizar.setEnabled(false);
        vista.btnAgregar.setEnabled(false);
    }

    void desbloquear3() {
        vista.btnListar.setEnabled(true);
        vista.btnNuevo.setEnabled(false);
        vista.Btn_reset.setEnabled(false);
        vista.btnEditar.setEnabled(true);
        vista.btnDelete.setEnabled(false);
        vista.btnActualizar.setEnabled(true);
        vista.btnAgregar.setEnabled(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.Btn_buscar) {
            Buscar(vista.Txt_buscar.getText(), vista.List_buscar.getSelectedItem().toString(), vista.TablaOpcion);
        }
        if (e.getSource() == vista.Btn_reset) {
            limpiarTabla();
            Listas data2 = new Listas(0,"Seleccione...");
            vista.List_tipop.removeAllItems();
            vista.List_tipop.addItem(data2);
            getLista();
            vista.List_buscar.setSelectedIndex(0);
            vista.Txt_buscar.setText("");
            vista.btnListar.setEnabled(true);
            bloquear();
        }
        if (e.getSource() == vista.btnListar) {
            desbloquear2();
            vista.List_buscar.setSelectedIndex(0);
            vista.Txt_buscar.setText("");
            vista.btnListar.setEnabled(true);
            limpiarTabla();
            listar(vista.TablaOpcion);
            nuevo();
        }
        if (e.getSource() == vista.btnAgregar) {
            Listas item = (Listas) vista.List_tipop.getSelectedItem();
            add(item.getId());
            listar(vista.TablaOpcion);
            nuevo();
        }
        if (e.getSource() == vista.btnEditar) {
            desbloquear3();
            Seleccionar();
        }
        if (e.getSource() == vista.btnActualizar) {
            Listas item = (Listas) vista.List_tipop.getSelectedItem();
            Actualizar(item.getId());
            listar(vista.TablaOpcion);
            desbloquear2();
            Listas data2 = new Listas(0,"Seleccione...");
            vista.List_tipop.removeAllItems();
            vista.List_tipop.addItem(data2);
            getLista();
            nuevo();

        }
        if (e.getSource() == vista.btnDelete) {
            delete();
            listar(vista.TablaOpcion);
            nuevo();
        }
        if (e.getSource() == vista.btnNuevo) {
            desbloquear1();
            nuevo();
        }
    }

    void nuevo() {
        vista.Txt_id.setText("");
        vista.Txt_nombre.setText("");
        vista.List_tipop.setSelectedIndex(0);
        vista.Txt_nombre.requestFocus();
    }

    public void Seleccionar() {
        int fila = vista.TablaOpcion.getSelectedRow();
        if (fila == -1) {
            JOptionPane.showMessageDialog(vista, "Debe Seleccionar Una fila..!!");
        } else {
            int id = Integer.parseInt((String) vista.TablaOpcion.getValueAt(fila, 0).toString());
            String username = (String) vista.TablaOpcion.getValueAt(fila, 1);
            String rol = (String) vista.TablaOpcion.getValueAt(fila, 2).toString();
            vista.Txt_id.setText("" + id);
            vista.Txt_nombre.setText(username);
            getSelect(rol);
        }
    }

    public void delete() {
        int fila = vista.TablaOpcion.getSelectedRow();
        if (fila == -1) {
            JOptionPane.showMessageDialog(vista, "Debe Seleccionar una Fila...!!!");
        } else {
            int id = Integer.parseInt((String) vista.TablaOpcion.getValueAt(fila, 0).toString());
            dao.Delete(id);
            System.out.println("El Reusltado es " + id);
            JOptionPane.showMessageDialog(vista, "Usuario Eliminado...!!!");
        }
        limpiarTabla();
    }

    public void add(int id) {
        int id_sub;
        String username = vista.Txt_nombre.getText();
        p.setId(id_sub = 0);
        p.setSublista(username);
        String id1 = String.valueOf(id);
        p.setId_lista(id1);
        int r = dao.agregar(p);
        if (r == 1) {
            JOptionPane.showMessageDialog(vista, "Opción Agregada con Exito.");
        } else {
            JOptionPane.showMessageDialog(vista, "Error");
        }
        limpiarTabla();
    }

    public void Actualizar(int id_lista) {
        if (vista.Txt_id.getText().equals("")) {
            JOptionPane.showMessageDialog(vista, "No se Identifica el Id debe selecionar la opcion Editar");
        } else {
            String username = vista.Txt_nombre.getText();
            int id = Integer.parseInt(vista.Txt_id.getText());
            p.setSublista(username);
            String id_lista1 = String.valueOf(id_lista);
            p.setId_lista(id_lista1);
            p.setId(id);
            int r = dao.Actualizar(p);
            if (r == 1) {
                JOptionPane.showMessageDialog(vista, "Opción Actualizada con Exito.");
            } else {
                JOptionPane.showMessageDialog(vista, "Error");
            }
        }
        limpiarTabla();
    }

    public void Buscar(String valorl, String filtrol, JTable tablalectura) {
        String[] titulos = {"ID", "NOMBRE OPCIÓN", "TIPO OPCIÓN"};
        String[] registros = new String[3];
        modelo = new DefaultTableModel(null, titulos);
        String SSQL = null;
        Connection conect = null;
        if (filtrol.equals("Nombre Opción")) {
            SSQL = "SELECT * "
                    + "FROM sublista "
                    + "LEFT JOIN listas ON sublista.id_lista = listas.id "
                    + "WHERE sublista.sublista LIKE '%" + valorl + "%' and sublista.estado = 1;";
        } else if (filtrol.equals("Tipo Opción")) {
            SSQL = "SELECT * "
                    + "FROM sublista "
                    + "LEFT JOIN listas ON sublista.id_lista = lista.id "
                    + "WHERE listas.lista LIKE '%" + valorl + "%' and sublista.estado = 1;";
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione algun Filtro", "Error", JOptionPane.ERROR_MESSAGE);
        }
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(SSQL);
            while (rs.next()) {
                registros[0] = rs.getString("sublista.id");
                registros[1] = rs.getString("sublista.sublista");
                registros[2] = rs.getString("listas.lista");
                modelo.addRow(registros);
            }
            tablalectura.setModel(modelo);
            vista.btnListar.setEnabled(false);
            vista.Btn_reset.setEnabled(true);
            vista.btnDelete.setEnabled(true);
            vista.btnEditar.setEnabled(true);
        } catch (SQLException e) {
            //JOptionPane.showMessageDialog(null, e, "Error durante el procedimiento", JOptionPane.ERROR_MESSAGE);
            System.out.print(e);
        }
    }

    public void listar(JTable tabla) {
        centrarCeldas(tabla);
        try {
            String[] titulos = {"ID", "NOMBRE OPCIÓN", "TIPO OPCIÓN"};
            String[] registros = new String[3];
            modelo = new DefaultTableModel(null, titulos);
            String cons
                    = ("SELECT * "
                    + "FROM sublista "
                    + "LEFT JOIN listas ON sublista.id_lista = listas.id "
                    + "where sublista.estado != 0 ");
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(cons);
            while (rs.next()) {
                registros[0] = rs.getString("sublista.id");
                registros[1] = rs.getString("sublista.sublista");
                registros[2] = rs.getString("listas.lista");
                modelo.addRow(registros);
            }
            vista.TablaOpcion.setModel(modelo);
            vista.TablaOpcion.getColumnModel().getColumn(0).setPreferredWidth(60);
            vista.TablaOpcion.getColumnModel().getColumn(1).setPreferredWidth(230);
            vista.TablaOpcion.getColumnModel().getColumn(2).setPreferredWidth(170);
        } catch (SQLException e) {
            Logger.getLogger(vista.getName()).log(Level.SEVERE, null, e);
        }

    }

    void centrarCeldas(JTable tabla) {
        DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();
        tcr.setHorizontalAlignment(SwingConstants.CENTER);
        for (int i = 0; i < vista.TablaOpcion.getColumnCount(); i++) {
            tabla.getColumnModel().getColumn(i).setCellRenderer(tcr);
        }
    }

    void limpiarTabla() {
        for (int i = 0; i < vista.TablaOpcion.getRowCount(); i++) {
            modelo.removeRow(i);
            i = i - 1;
        }
    }
}
