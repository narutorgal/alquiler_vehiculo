package com.alquiler.controlador;

import com.alquiler.modelo.User;
import com.alquiler.modelo.UserDao;
import com.alquiler.vista.vistausuario;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import com.alquiler.modelo.Conexion;
import com.alquiler.modelo.Sublista;
import com.alquiler.modelo.SublistaDao;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

public class UserController implements ActionListener {

    Conexion cc = new Conexion();
    Connection cn = cc.getConnection();
    UserDao dao = new UserDao();
    SublistaDao daos = new SublistaDao();
    User p = new User();
    vistausuario vista = new vistausuario();
    DefaultTableModel modelo = new DefaultTableModel();
    Sublista sublista = new Sublista();

    public UserController(vistausuario v) {
        this.vista = v;
        this.vista.btnListar.addActionListener(this);
        this.vista.btnAgregar.addActionListener(this);
        this.vista.btnEditar.addActionListener(this);
        this.vista.btnDelete.addActionListener(this);
        this.vista.btnActualizar.addActionListener(this);
        this.vista.btnNuevo.addActionListener(this);
        this.vista.Btn_reset.addActionListener(this);
        this.vista.Btn_buscar.addActionListener(this);
        vista.List_tipousuario.addActionListener(this);
        bloquear();
        getLista();
        System.out.println("-----");
    }

    public final void getLista() {
        Vector<Sublista> oListaObjetoParaComboBox = daos.ListaDeObjetosParaComboBox3();
        System.out.println("Vector generado de roles completos " + oListaObjetoParaComboBox);
        if (oListaObjetoParaComboBox.size() > 0) {
            System.out.println("Tamaño del vector de roles completo " + oListaObjetoParaComboBox.size());
            for (int i = 0; i < oListaObjetoParaComboBox.size(); i = i + 1) {
                Sublista contar = oListaObjetoParaComboBox.get(i);
                System.out.println("Tamaño contador " + i + " Generando ID... " + contar.getId());
                int id = contar.getId();
                String id_lista = contar.getId_lista();
                String nombre = contar.getSublista();
                Sublista oItem = new Sublista(id, id_lista, nombre);
                vista.List_tipousuario.addItem(oItem);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No hay marcas registrados en la Base de datos", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);
        }
    }

    public final void getListaselect(String tipousuario) {
        Vector<Sublista> oListaObjetoParaComboBox = daos.getTipo1(tipousuario);
        if (oListaObjetoParaComboBox.size() > 0) {
            for (int i = 0; i < oListaObjetoParaComboBox.size(); i = i + 1) {
                Sublista contar = oListaObjetoParaComboBox.get(i);
                Sublista oItem = new Sublista(contar.getId(), contar.getId_lista(), contar.getSublista());
                vista.List_tipousuario.removeAllItems();
                vista.List_tipousuario.addItem(oItem);
                vista.List_tipousuario.setSelectedIndex(i++);
                getLista();
            }
        } else {
            JOptionPane.showMessageDialog(null, "No hay roles registrados en la Base de datos", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);
        }
    }

    void bloquear() {
        vista.Txt_nombre.setEnabled(false);
        vista.Txt_password.setEnabled(false);
        vista.List_tipousuario.setEnabled(false);
        vista.btnListar.setEnabled(true);
        vista.btnNuevo.setEnabled(true);
        vista.Btn_reset.setEnabled(false);
        vista.btnEditar.setEnabled(false);
        vista.btnDelete.setEnabled(false);
        vista.btnActualizar.setEnabled(false);
        vista.btnAgregar.setEnabled(false);
    }

    void desbloquear1() {
        vista.btnListar.setEnabled(true);
        vista.btnNuevo.setEnabled(false);
        vista.Btn_reset.setEnabled(false);
        vista.btnEditar.setEnabled(false);
        vista.btnDelete.setEnabled(false);
        vista.btnActualizar.setEnabled(false);
        vista.btnAgregar.setEnabled(true);
    }

    void desbloquear2() {
        vista.btnListar.setEnabled(false);
        vista.btnNuevo.setEnabled(false);
        vista.Btn_reset.setEnabled(false);
        vista.btnEditar.setEnabled(true);
        vista.btnDelete.setEnabled(true);
        vista.btnActualizar.setEnabled(false);
        vista.btnAgregar.setEnabled(false);
    }

    void desbloquear3() {
        vista.btnListar.setEnabled(true);
        vista.btnNuevo.setEnabled(false);
        vista.Btn_reset.setEnabled(false);
        vista.btnEditar.setEnabled(true);
        vista.btnDelete.setEnabled(false);
        vista.btnActualizar.setEnabled(true);
        vista.btnAgregar.setEnabled(false);
    }
    void bloquear1() {
        vista.List_tipousuario.setEnabled(false);
        vista.Txt_nombre.setEnabled(false);
        vista.Txt_password.setEnabled(false);
        vista.btnListar.setEnabled(true);
        vista.btnNuevo.setEnabled(true);
        vista.Btn_reset.setEnabled(false);
        vista.btnEditar.setEnabled(true);
        vista.btnDelete.setEnabled(true);
        vista.btnActualizar.setEnabled(false);
        vista.btnAgregar.setEnabled(false);
        vista.btnListar.setEnabled(false);
    }
    void limpiar(){
        vista.Txt_id.setText("");
        vista.Txt_nombre.setText("");
        vista.Txt_password.setText("");
        vista.List_tipousuario.setSelectedIndex(0);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.Btn_buscar) {
            Buscar(vista.Txt_buscar.getText(), vista.List_buscar.getSelectedItem().toString(), vista.Tablauser);
        }
        if (e.getSource() == vista.Btn_reset) {
            limpiarTabla();
            
            vista.List_buscar.setSelectedIndex(0);
            vista.Txt_buscar.setText("");
            vista.btnListar.setEnabled(true);
            bloquear();
        }
        if (e.getSource() == vista.btnListar) {
            desbloquear2();
            vista.List_buscar.setSelectedIndex(0);
            vista.Txt_buscar.setText("");
            vista.btnListar.setEnabled(true);
            limpiarTabla();
            listar(vista.Tablauser);
            nuevo();
        }
        if (e.getSource() == vista.btnAgregar) {
            Sublista item = (Sublista) vista.List_tipousuario.getSelectedItem();
            add(item.getId());
            listar(vista.Tablauser);
            nuevo();
        }
        if (e.getSource() == vista.btnEditar) {
            desbloquear3();
            Seleccionar();
        }
        if (e.getSource() == vista.btnActualizar) {
            Sublista item = (Sublista) vista.List_tipousuario.getSelectedItem();
            Actualizar(item.getId());
            listar(vista.Tablauser);
            bloquear1();
            Sublista data2 = new Sublista(0, "0", "Seleccione...");
            vista.List_tipousuario.removeAllItems();
            vista.List_tipousuario.addItem(data2);
            getLista();
            limpiar();

        }
        if (e.getSource() == vista.btnDelete) {
            delete();
            listar(vista.Tablauser);
            nuevo();
        }
        if (e.getSource() == vista.btnNuevo) {
            desbloquear1();
            nuevo();
        }
    }

    void nuevo() {
        vista.Txt_nombre.setEnabled(true);
        vista.Txt_password.setEnabled(true);
        vista.List_tipousuario.setEnabled(true);
        vista.Txt_id.setText("");
        vista.Txt_nombre.setText("");
        vista.Txt_password.setText("");
        vista.List_tipousuario.setSelectedIndex(0);
        vista.Txt_nombre.requestFocus();
    }

    public void Seleccionar() {
        int fila = vista.Tablauser.getSelectedRow();
        if (fila == -1) {
            JOptionPane.showMessageDialog(vista, "Debe Seleccionar Una fila..!!");
        } else {
            int id = Integer.parseInt((String) vista.Tablauser.getValueAt(fila, 0).toString());
            String username = (String) vista.Tablauser.getValueAt(fila, 1);
            String password = (String) vista.Tablauser.getValueAt(fila, 2);
            String tipousuario = (String) vista.Tablauser.getValueAt(fila, 3);
            vista.Txt_id.setText("" + id);
            vista.Txt_nombre.setText(username);
            vista.Txt_password.setText(password);
            getListaselect(tipousuario);
        }
    }

    public void delete() {
        int fila = vista.Tablauser.getSelectedRow();
        if (fila == -1) {
            JOptionPane.showMessageDialog(vista, "Debe Seleccionar una Fila...!!!");
        } else {
            int id = Integer.parseInt((String) vista.Tablauser.getValueAt(fila, 0).toString());
            dao.Delete(id);
            System.out.println("El Reusltado es " + id);
            JOptionPane.showMessageDialog(vista, "Usuario Eliminado...!!!");
        }
        limpiarTabla();
    }

    public void add(int id) {
        String username = vista.Txt_nombre.getText();
        String password = vista.Txt_password.getText();
        p.setUsername(username);
        p.setPassword(password);
        p.setId_tipousuario(id);
        int r = dao.agregar(p);
        if (r == 1) {
            JOptionPane.showMessageDialog(vista, "Usuario Agregado con Exito.");
        } else {
            JOptionPane.showMessageDialog(vista, "Error");
        }
        limpiarTabla();
    }

    public void Actualizar(int id_lista) {
        if (vista.Txt_id.getText().equals("")) {
            JOptionPane.showMessageDialog(vista, "No se Identifica el Id debe selecionar la opcion Editar");
        } else {
            String username = vista.Txt_nombre.getText();
            String password = vista.Txt_password.getText();
            int id = Integer.parseInt(vista.Txt_id.getText());
            p.setUsername(username);
            p.setPassword(password);
            p.setId_tipousuario(id_lista);
            p.setId_usuario(id);
            int r = dao.Actualizar(p);
            if (r == 1) {
                JOptionPane.showMessageDialog(vista, "Usuario Actualizado con Exito.");
            } else {
                JOptionPane.showMessageDialog(vista, "Error");
            }
        }
        limpiarTabla();
    }

    public void Buscar(String valorl, String filtrol, JTable tablalectura) {
        String[] titulos = {"ID", "USERNAME", "PASSWORD", "TIPO USUARIO"};
        String[] registros = new String[4];
        modelo = new DefaultTableModel(null, titulos);
        String SSQL = null;
        Connection conect = null;
        if (filtrol.equals("Username")) {
            SSQL = "SELECT * "
                    + "FROM usuario "
                    + "LEFT JOIN sublista ON usuario.id_tipousuario = sublista.id "
                    + "WHERE username LIKE '%" + valorl + "%' and usuario.estado = 1;";
        } else if (filtrol.equals("Tipo Usuario")) {
            SSQL = "SELECT * "
                    + "FROM usuario "
                    + "LEFT JOIN sublista ON usuario.id_tipousuario = sublista.id "
                    + "WHERE sublista.sublista LIKE '%" + valorl + "%' and usuario.estado = 1;";
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione algun Filtro", "Error", JOptionPane.ERROR_MESSAGE);
        }
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(SSQL);
            while (rs.next()) {
                registros[0] = rs.getString("usuario.id_usuario");
                registros[1] = rs.getString("username");
                registros[2] = rs.getString("password");
                registros[3] = rs.getString("sublista.sublista");
                modelo.addRow(registros);
            }
            tablalectura.setModel(modelo);
            vista.btnListar.setEnabled(false);
            vista.Btn_reset.setEnabled(true);
            vista.btnDelete.setEnabled(true);
            vista.btnEditar.setEnabled(true);
        } catch (SQLException e) {
            //JOptionPane.showMessageDialog(null, e, "Error durante el procedimiento", JOptionPane.ERROR_MESSAGE);
            System.out.print(e);
        }
    }

    public void listar(JTable tabla) {
        centrarCeldas(tabla);
        try {
            String[] titulos = {"ID", "USERNAME", "PASSWORD", "TIPO USUARIO"};
            String[] registros = new String[10];
            modelo = new DefaultTableModel(null, titulos);
            String cons
                    = ("SELECT usuario.id_usuario, usuario.username, usuario.password, sublista.sublista as tipo FROM usuario "
                    + "LEFT JOIN sublista ON usuario.id_tipousuario = sublista.id where usuario.estado != 0 ");
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(cons);
            while (rs.next()) {
                registros[0] = rs.getString("usuario.id_usuario");
                registros[1] = rs.getString("usuario.username");
                registros[2] = rs.getString("usuario.password");
                registros[3] = rs.getString("tipo");
                modelo.addRow(registros);
            }
            vista.Tablauser.setModel(modelo);
            vista.Tablauser.getColumnModel().getColumn(0).setPreferredWidth(60);
            vista.Tablauser.getColumnModel().getColumn(1).setPreferredWidth(230);
            vista.Tablauser.getColumnModel().getColumn(2).setPreferredWidth(170);
            vista.Tablauser.getColumnModel().getColumn(3).setPreferredWidth(170);
        } catch (SQLException e) {
            Logger.getLogger(vista.getName()).log(Level.SEVERE, null, e);
        }

    }

    void centrarCeldas(JTable tabla) {
        DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();
        tcr.setHorizontalAlignment(SwingConstants.CENTER);
        for (int i = 0; i < vista.Tablauser.getColumnCount(); i++) {
            tabla.getColumnModel().getColumn(i).setCellRenderer(tcr);
        }
    }

    void limpiarTabla() {
        for (int i = 0; i < vista.Tablauser.getRowCount(); i++) {
            modelo.removeRow(i);
            i = i - 1;
        }
    }
}
