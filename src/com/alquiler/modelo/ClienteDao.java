package com.alquiler.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class ClienteDao {

    PreparedStatement ps;
    ResultSet rs;
    Connection con;
    Conexion conectar = new Conexion();
    Cliente p = new Cliente();

    public List listar() {
        List<Cliente> datos = new ArrayList<>();
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement("select cliente.id,"
                    + " cliente.cedula,"
                    + " cliente.nombre,"
                    + " cliente.apellido,"
                    + " cliente.telefono,"
                    + " cliente.direccion,"
                    + " cliente.email from cliente where estado != 0");
            rs = ps.executeQuery();
            while (rs.next()) {
                Cliente p = new Cliente();
                p.setId(rs.getInt(1));
                p.setCedula(rs.getInt(2));
                p.setNombre(rs.getString(3));
                p.setApellido(rs.getString(4));
                p.setTelefono(rs.getString(5));
                p.setDireccion(rs.getString(6));
                p.setEmail(rs.getString(7));
                datos.add(p);
            }
        } catch (Exception e) {
        }
        return datos;
    }

    public int agregar(Cliente per) {
        int r = 0;
        String sql = "insert into cliente(id,cedula,nombre,apellido,telefono,direccion,email)values(?,?,?,?,?,?,?)";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, per.getId());
            ps.setInt(2, per.getCedula());
            ps.setString(3, per.getNombre());
            ps.setString(4, per.getApellido());
            ps.setString(5, per.getTelefono());
            ps.setString(6, per.getDireccion());
            ps.setString(7, per.getEmail());
            r = ps.executeUpdate();
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            System.out.print(e);
            JOptionPane.showMessageDialog(null, sql);
            System.out.print(sql);
        }
        return r;
    }

    public int Actualizar(Cliente per) {
        int r = 0;
        String sql = "update cliente set cedula=?,nombre=?,apellido=?,telefono=?,direccion=?,email=? where Id=?";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, per.getCedula());
            ps.setString(2, per.getNombre());
            ps.setString(3, per.getApellido());
            ps.setString(4, per.getTelefono());
            ps.setString(5, per.getDireccion());
            ps.setString(6, per.getEmail());
            ps.setInt(7, per.getId());
            r = ps.executeUpdate();
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
        }
        return r;
    }

    public int Delete(int id) {
        int r = 0;
        String sql = "update cliente set estado = 0 where Id=" + id;
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            r = ps.executeUpdate();
        } catch (Exception e) {
        }
        return r;
    }

    public Vector<Cliente> getCliente() {
        String sql = "SELECT * "
                        + "FROM cliente "
                        + "WHERE estado != 0";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            Vector<Cliente> data = new Vector<Cliente>();
            if (rs.next()) {
                do {
                    Cliente p = new Cliente();
                    p.setId(rs.getInt(1));
                    p.setCedula(rs.getInt(2));
                    p.setNombre(rs.getString(3));
                    p.setApellido(rs.getString(4));
                    p.setTelefono(rs.getString(5));
                    p.setDireccion(rs.getString(6));
                    p.setEmail(rs.getString(7));
                    p.setEstado(rs.getString(8));
                    data.addElement(p);
                } while (rs.next());
                return data;

            } else {
                return null;
            }
        } catch (Exception ex) {
            System.out.print(ex);
            return null;
        }
    }

    public Vector<Cliente> getCliente1(String cedula) {
        String sql = "SELECT * "
                        + "FROM cliente "
                        + "WHERE estado != 0 and cliente.cedula ="+cedula+"";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            Vector<Cliente> data = new Vector<Cliente>();
            if (rs.next()) {
                do {
                    Cliente p = new Cliente();
                    p.setId(rs.getInt(1));
                    p.setCedula(rs.getInt(2));
                    p.setNombre(rs.getString(3));
                    p.setApellido(rs.getString(4));
                    p.setTelefono(rs.getString(5));
                    p.setDireccion(rs.getString(6));
                    p.setEmail(rs.getString(7));
                    p.setEstado(rs.getString(8));
                    data.addElement(p);
                } while (rs.next());
                return data;

            } else {
                return null;
            }
        } catch (Exception ex) {
            System.out.print(ex);
            System.out.print(sql);
            return null;
        }
    }
}
