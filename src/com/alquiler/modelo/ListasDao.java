
package com.alquiler.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.JOptionPane;

public class ListasDao {

    PreparedStatement ps;
    ResultSet rs;
    Connection con;
    Conexion conectar = new Conexion();
    Listas s = new Listas();

    public Vector<Listas> getTipos() {
        String sql = "SELECT id, lista, estado "
                + "FROM listas "
                + "WHERE listas.estado !=0";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            Vector<Listas> data = new Vector<Listas>();
            if (rs.next()) {
                do {
                    Listas p = new Listas();
                    p.setId(rs.getInt(1));
                    p.setLista(rs.getString(2));
                    p.setEstado(rs.getString(3));
                    data.addElement(p);
                } while (rs.next());
                return data;

            } else {
                return null;
            }
        } catch (Exception ex) {
            System.out.print(ex);
            return null;
        }
    }
    public Vector<Listas> getRoles1(String id) {
        String sql = "SELECT id, lista, estado "
                + "FROM listas "
                + "WHERE listas.estado !=0 and listas.lista = '"+id+"'";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            Vector<Listas> data = new Vector<Listas>();
            if (rs.next()) {
                do {
                    Listas p = new Listas();
                    p.setId(rs.getInt(1));
                    p.setLista(rs.getString(2));
                    p.setEstado(rs.getString(3));
                    data.addElement(p);
                } while (rs.next());
                return data;

            } else {
                return null;
            }
        } catch (Exception ex) {
            System.out.print(ex);
            System.out.print(sql);
            return null;
        }
    }
}
