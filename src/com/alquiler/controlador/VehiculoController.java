package com.alquiler.controlador;

import com.alquiler.modelo.Vehiculo;
import com.alquiler.modelo.VehiculoDao;
import com.alquiler.vista.vistavehiculo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import com.alquiler.modelo.Conexion;
import com.alquiler.modelo.Sublista;
import com.alquiler.modelo.SublistaDao;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;
import javax.swing.JComboBox;

public class VehiculoController implements ActionListener {

    Conexion cc = new Conexion();
    Connection cn = cc.getConnection();
    VehiculoDao dao = new VehiculoDao();
    SublistaDao daos = new SublistaDao();
    Vehiculo p = new Vehiculo();
    vistavehiculo vista = new vistavehiculo();
    DefaultTableModel modelo = new DefaultTableModel();
    Sublista sublista = new Sublista();

    public VehiculoController(vistavehiculo v) {
        this.vista = v;
        this.vista.btnListar.addActionListener(this);
        this.vista.btnAgregar.addActionListener(this);
        this.vista.btnEditar.addActionListener(this);
        this.vista.btnDelete.addActionListener(this);
        this.vista.btnActualizar.addActionListener(this);
        this.vista.btnNuevo.addActionListener(this);
        this.vista.Btn_reset.addActionListener(this);
        this.vista.Btn_buscar.addActionListener(this);
        bloquear();
        mostrarmarca();
        mostrarlinea();
    }

    public void mostrarmarca() {
        Vector<Sublista> oListaObjetoParaComboBox = daos.ListaDeObjetosParaComboBox();
        if (oListaObjetoParaComboBox.size() > 0) {
            for (int i = 0; i < oListaObjetoParaComboBox.size(); i = i + 1) {
                Sublista contar = oListaObjetoParaComboBox.get(i);
                Sublista oItem = new Sublista(contar.getId(), contar.getId_lista(), contar.getSublista());
                vista.List_marca.addItem(oItem);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No hay marcas registrados en la Base de datos", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void mostrarlinea() {
        Vector<Sublista> oListaObjetoParaComboBox = daos.ListaDeObjetosParaComboBox2();
        if (oListaObjetoParaComboBox.size() > 0) {
            for (int i = 0; i < oListaObjetoParaComboBox.size(); i = i + 1) {
                Sublista contar = oListaObjetoParaComboBox.get(i);
                Sublista oItem = new Sublista(contar.getId(), contar.getId_lista(), contar.getSublista());
                vista.List_linea.addItem(oItem);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No hay marcas registrados en la Base de datos", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void getSelect(String marcas, String lineas) {
        if (marcas != null) {
            Vector<Sublista> oListaObjetoParaComboBox = daos.getMarcas1(marcas);
            if (oListaObjetoParaComboBox.size() > 0) {
                for (int i = 0; i < oListaObjetoParaComboBox.size(); i++) {
                    Sublista contar = oListaObjetoParaComboBox.get(i);
                    Sublista oItem1 = new Sublista(contar.getId(), contar.getId_lista(), contar.getSublista());
                    vista.List_marca.removeAllItems();
                    vista.List_marca.addItem(oItem1);
                    vista.List_marca.setSelectedIndex(i++);
                    mostrarmarca();
                }
            } else {
                JOptionPane.showMessageDialog(null, "No hay marcas registrados en la Base de datos", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            System.out.println("no envia nada");
        }
        if (lineas != null) {
            Vector<Sublista> oListaObjetoParaComboBox = daos.getLineas1(lineas);
            if (oListaObjetoParaComboBox.size() > 0) {
                for (int i = 0; i < oListaObjetoParaComboBox.size(); i++) {
                    Sublista contar = oListaObjetoParaComboBox.get(i);
                    Sublista oItem = new Sublista(contar.getId(), contar.getId_lista(), contar.getSublista());
                    vista.List_linea.removeAllItems();
                    vista.List_linea.addItem(oItem);
                    vista.List_linea.setSelectedIndex(i);
                    mostrarlinea();
                }
            } else {
                JOptionPane.showMessageDialog(null, "No hay marcas registrados en la Base de datos", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            System.out.println("no envia nada");
        }
    }

    public void listar(JTable tabla) {
        centrarCeldas(tabla);
        modelo = (DefaultTableModel) tabla.getModel();
        tabla.setModel(modelo);
        List<Vehiculo> lista = dao.listar();
        Object[] objeto = new Object[5];
        for (int i = 0; i < lista.size(); i++) {
            objeto[0] = lista.get(i).getId();
            objeto[1] = lista.get(i).getPlaca();
            objeto[2] = lista.get(i).getMarca();
            objeto[3] = lista.get(i).getLinea();
            objeto[4] = lista.get(i).getEstado();
            modelo.addRow(objeto);
        }
        tabla.setRowHeight(35);
        tabla.setRowMargin(10);
    }

    void bloquear() {
        vista.List_linea.setEnabled(false);
        vista.List_marca.setEnabled(false);
        vista.Txt_placa.setEnabled(false);
        vista.btnListar.setEnabled(true);
        vista.btnNuevo.setEnabled(true);
        vista.Btn_reset.setEnabled(false);
        vista.btnEditar.setEnabled(false);
        vista.btnDelete.setEnabled(false);
        vista.btnActualizar.setEnabled(false);
        vista.btnAgregar.setEnabled(false);
    }

    void desbloquear1() {
        vista.btnListar.setEnabled(true);
        vista.btnNuevo.setEnabled(false);
        vista.Btn_reset.setEnabled(false);
        vista.btnEditar.setEnabled(false);
        vista.btnDelete.setEnabled(false);
        vista.btnActualizar.setEnabled(false);
        vista.btnAgregar.setEnabled(true);
    }

    void desbloquear2() {
        vista.btnListar.setEnabled(false);
        vista.btnNuevo.setEnabled(true);
        vista.Btn_reset.setEnabled(false);
        vista.btnEditar.setEnabled(true);
        vista.btnDelete.setEnabled(true);
        vista.btnActualizar.setEnabled(false);
        vista.btnAgregar.setEnabled(false);
    }

    void desbloquear3() {
        vista.List_linea.setEnabled(true);
        vista.List_marca.setEnabled(true);
        vista.Txt_placa.setEnabled(true);
        vista.btnListar.setEnabled(true);
        vista.btnNuevo.setEnabled(false);
        vista.Btn_reset.setEnabled(false);
        vista.btnEditar.setEnabled(true);
        vista.btnDelete.setEnabled(false);
        vista.btnActualizar.setEnabled(true);
        vista.btnAgregar.setEnabled(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == vista.Btn_buscar) {
            Buscar(vista.Txt_buscar.getText(), vista.List_buscar.getSelectedItem().toString(), vista.Tablaauto);
        }
        if (e.getSource() == vista.Btn_reset) {
            limpiarTabla();
            vista.List_buscar.setSelectedIndex(0);
            vista.Txt_buscar.setText("");
            vista.btnListar.setEnabled(true);
            vista.Txt_placa.requestFocus();
        }
        if (e.getSource() == vista.btnListar) {
            desbloquear2();
            limpiarTabla();
            listar(vista.Tablaauto);
            nuevo();
        }
        if (e.getSource() == vista.btnAgregar) {
            Sublista item = (Sublista) vista.List_linea.getSelectedItem();
            Sublista item2 = (Sublista) vista.List_marca.getSelectedItem();
            add(item.getId(), item2.getId());
            listar(vista.Tablaauto);
            bloquear();
            nuevo();
        }
        if (e.getSource() == vista.btnEditar) {
            desbloquear3();
            int fila = vista.Tablaauto.getSelectedRow();
            if (fila == -1) {
                JOptionPane.showMessageDialog(vista, "Debee Seleccionar Una fila..!!");
            } else {
                String estado = (String) vista.Tablaauto.getValueAt(fila, 4);
                if (estado.equals("Disponible")) {
                    int id = Integer.parseInt((String) vista.Tablaauto.getValueAt(fila, 0).toString());
                    String placa = (String) vista.Tablaauto.getValueAt(fila, 1);
                    String marca = (String) vista.Tablaauto.getValueAt(fila, 2).toString();
                    String linea = (String) vista.Tablaauto.getValueAt(fila, 3).toString();
                    vista.Txt_id.setText("" + id);
                    vista.Txt_placa.setText(placa);
                    getSelect(marca, linea);
                } else {
                    JOptionPane.showMessageDialog(vista, "El vehiculo se encuentra alquilado y no se puede modificar");
                    bloquear();
                    nuevo();
                }
            }
        }
        if (e.getSource() == vista.btnActualizar) {
            Sublista item = (Sublista) vista.List_linea.getSelectedItem();
            Sublista item2 = (Sublista) vista.List_marca.getSelectedItem();
            Actualizar(item.getId(), item2.getId());
            listar(vista.Tablaauto);
            bloquear();
            nuevo();
            Sublista data2 = new Sublista(0, "0", "Seleccione...");
            vista.List_linea.removeAllItems();
            vista.List_marca.removeAllItems();
            vista.List_linea.addItem(data2);
            vista.List_marca.addItem(data2);
            mostrarlinea();
            mostrarmarca();
        }
        if (e.getSource() == vista.btnDelete) {
            delete();
            limpiarTabla();
            nuevo();
            bloquear();
        }
        if (e.getSource() == vista.btnNuevo) {
            desbloquear1();
            nuevo();
        }
    }

    void nuevo() {
        vista.List_linea.setEnabled(true);
        vista.List_marca.setEnabled(true);
        vista.Txt_placa.setEnabled(true);
        vista.Txt_id.setText("");
        vista.Txt_placa.setText("");
        vista.List_marca.setSelectedIndex(0);
        vista.List_linea.setSelectedIndex(0);
        vista.Txt_placa.requestFocus();
    }

    public void Buscar(String valorl, String filtrol, JTable tablalectura) {
        String[] titulos = {"ID", "PLACA", "LINEA", "MARCA", "ESTADO"};
        String[] registros = new String[5];
        modelo = new DefaultTableModel(null, titulos);
        String SSQL = null;
        Connection conect = null;
        if (filtrol.equals("Placa")) {
            SSQL = "SELECT auto.id, auto.placa,linea.sublista, marca.sublista, "
                    + "CASE WHEN auto.estado= 1 THEN 'Disponible' "
                    + "WHEN auto.estado= 2 THEN 'Alquilado' "
                    + "Else 'Eliminado' END as estado_vehiculo "
                    + "FROM auto "
                    + "LEFT JOIN sublista linea ON auto.linea = linea.id "
                    + "LEFT JOIN sublista marca ON auto.marca = marca.id "
                    + "WHERE placa LIKE '%" + valorl + "%' and auto.estado = 1;";
        } else if (filtrol.equals("Linea")) {
            SSQL = "SELECT auto.id, auto.placa,linea.sublista, marca.sublista, "
                    + "CASE WHEN auto.estado= 1 THEN 'Disponible' "
                    + "WHEN auto.estado= 2 THEN 'Alquilado' "
                    + "Else 'Eliminado' END as estado_vehiculo "
                    + "FROM auto "
                    + "LEFT JOIN sublista linea ON auto.linea = linea.id "
                    + "LEFT JOIN sublista marca ON auto.marca = marca.id "
                    + "WHERE linea.sublista LIKE '%" + valorl + "%' and auto.estado = 1;";
        } else if (filtrol.equals("Marca")) {
            SSQL = "SELECT auto.id, auto.placa,linea.sublista, marca.sublista, "
                    + "CASE WHEN auto.estado= 1 THEN 'Disponible' "
                    + "WHEN auto.estado= 2 THEN 'Alquilado' "
                    + "Else 'Eliminado' END as estado_vehiculo "
                    + "FROM auto "
                    + "LEFT JOIN sublista linea ON auto.linea = linea.id "
                    + "LEFT JOIN sublista marca ON auto.marca = marca.id "
                    + "WHERE marca.sublista LIKE '%" + valorl + "%' and auto.estado = 1;";
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione algun Filtro", "Error", JOptionPane.ERROR_MESSAGE);
        }
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(SSQL);
            while (rs.next()) {
                registros[0] = rs.getString("auto.id");
                registros[1] = rs.getString("placa");
                registros[2] = rs.getString("linea.sublista");
                registros[3] = rs.getString("marca.sublista");
                registros[4] = rs.getString("estado_vehiculo");
                modelo.addRow(registros);
            }
            tablalectura.setModel(modelo);
            vista.btnListar.setEnabled(false);
            vista.Btn_reset.setEnabled(true);
        } catch (SQLException e) {
            //JOptionPane.showMessageDialog(null, e, "Error durante el procedimiento", JOptionPane.ERROR_MESSAGE);
            System.out.print(e);
        }
    }

    public void delete() {
        int fila = vista.Tablaauto.getSelectedRow();
        if (fila == -1) {
            JOptionPane.showMessageDialog(vista, "Debe Seleccionar una Fila...!!!");
        } else {
            int id = Integer.parseInt((String) vista.Tablaauto.getValueAt(fila, 0).toString());
            dao.Delete(id);
            System.out.println("El Reusltado es" + id);
            JOptionPane.showMessageDialog(vista, "Usuario Eliminado...!!!");
        }
        limpiarTabla();
    }

    public void add(int linea, int marca) {
        int id;
        String placa = vista.Txt_placa.getText();
        String marca1 = String.valueOf(marca);
        String linea1 = String.valueOf(linea);
        p.setId(id = 0);
        p.setPlaca(placa);
        p.setLinea(linea1);
        p.setMarca(marca1);
        int r = dao.agregar(p);
        if (r == 1) {
            JOptionPane.showMessageDialog(vista, "Vehiculo Agregado con Exito.");
        } else {
            JOptionPane.showMessageDialog(vista, "Error");
        }
        limpiarTabla();
    }

    public void Actualizar(int linea, int marca) {
        if (vista.Txt_id.getText().equals("")) {
            JOptionPane.showMessageDialog(vista, "No se Identifica el Id debe selecionar la opcion Editar");
        } else {
            String placa = vista.Txt_placa.getText();
            int id = Integer.parseInt(vista.Txt_id.getText());
            p.setPlaca(placa);
            String marca1 = String.valueOf(marca);
            String linea1 = String.valueOf(linea);
            p.setLinea(linea1);
            p.setMarca(marca1);
            p.setId(id);
            int r = dao.Actualizar(p);
            if (r == 1) {
                JOptionPane.showMessageDialog(vista, "Vehiculo Actualizado con Exito.");
            } else {
                JOptionPane.showMessageDialog(vista, "Error");
            }
        }
        limpiarTabla();
    }

    void centrarCeldas(JTable tabla) {
        DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();
        tcr.setHorizontalAlignment(SwingConstants.CENTER);
        for (int i = 0; i < vista.Tablaauto.getColumnCount(); i++) {
            tabla.getColumnModel().getColumn(i).setCellRenderer(tcr);
        }
    }

    public void limpiarTabla() {
        for (int i = 0; i < vista.Tablaauto.getRowCount(); i++) {
            modelo.removeRow(i);
            i = i - 1;
        }
    }
}
