package com.alquiler.controlador;

import com.alquiler.modelo.Cliente;
import com.alquiler.modelo.ClienteDao;
import com.alquiler.modelo.Conexion;
import com.alquiler.vista.vistacliente;
import com.alquiler.vista.vistaindex;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class ClienteController implements ActionListener {

    Conexion cc = new Conexion();
    Connection cn = cc.getConnection();
    ClienteDao dao = new ClienteDao();
    Cliente p = new Cliente();
    vistacliente vista = new vistacliente();
    vistaindex index = new vistaindex();
    DefaultTableModel modelo = new DefaultTableModel();

    public ClienteController(vistacliente v) {
        this.vista = v;
        this.vista.btnListar.addActionListener(this);
        this.vista.btnAgregar.addActionListener(this);
        this.vista.btnEditar.addActionListener(this);
        this.vista.btnDelete.addActionListener(this);
        this.vista.btnActualizar.addActionListener(this);
        this.vista.btnNuevo.addActionListener(this);
        this.vista.Btn_reset.addActionListener(this);
        this.vista.Btn_buscar.addActionListener(this);
        bloquear();
    }

    void bloquear() {
        vista.btnListar.setEnabled(true);
        vista.btnNuevo.setEnabled(true);
        vista.Btn_reset.setEnabled(false);
        vista.btnEditar.setEnabled(false);
        vista.btnDelete.setEnabled(false);
        vista.btnActualizar.setEnabled(false);
        vista.btnAgregar.setEnabled(false);
    }

    void desbloquear1() {
        vista.btnListar.setEnabled(true);
        vista.btnNuevo.setEnabled(false);
        vista.Btn_reset.setEnabled(false);
        vista.btnEditar.setEnabled(false);
        vista.btnDelete.setEnabled(false);
        vista.btnActualizar.setEnabled(false);
        vista.btnAgregar.setEnabled(true);
    }

    void desbloquear2() {
        vista.btnListar.setEnabled(false);
        vista.btnNuevo.setEnabled(true);
        vista.Btn_reset.setEnabled(false);
        vista.btnEditar.setEnabled(true);
        vista.btnDelete.setEnabled(true);
        vista.btnActualizar.setEnabled(false);
        vista.btnAgregar.setEnabled(false);
    }

    void desbloquear3() {
        vista.btnListar.setEnabled(true);
        vista.btnNuevo.setEnabled(false);
        vista.Btn_reset.setEnabled(false);
        vista.btnEditar.setEnabled(true);
        vista.btnDelete.setEnabled(false);
        vista.btnActualizar.setEnabled(true);
        vista.btnAgregar.setEnabled(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.Btn_buscar) {
            Buscar(vista.Txt_buscar.getText(), vista.List_buscar.getSelectedItem().toString(), vista.tabla);
        }
        if (e.getSource() == vista.Btn_reset) {
            limpiarTabla();
            vista.List_buscar.setSelectedIndex(0);
            vista.Txt_buscar.setText("");
            vista.btnListar.setEnabled(true);
        }
        if (e.getSource() == vista.btnListar) {
            desbloquear2();
            limpiarTabla();
            listar(vista.tabla);
            nuevo();
        }
        if (e.getSource() == vista.btnAgregar) {
            add();
            listar(vista.tabla);
            bloquear();
            nuevo();
        }
        if (e.getSource() == vista.btnEditar) {
            desbloquear3();
            int fila = vista.tabla.getSelectedRow();
            if (fila == -1) {
                JOptionPane.showMessageDialog(vista, "Debee Seleccionar Una fila..!!");
            } else {
                int id = Integer.parseInt((String) vista.tabla.getValueAt(fila, 0).toString());
                int cedula = Integer.parseInt((String) vista.tabla.getValueAt(fila, 1).toString());
                String nombre = (String) vista.tabla.getValueAt(fila, 2);
                String apellido = (String) vista.tabla.getValueAt(fila, 3);
                String telefono = (String) vista.tabla.getValueAt(fila, 4);
                String direccion = (String) vista.tabla.getValueAt(fila, 5);
                String email = (String) vista.tabla.getValueAt(fila, 6);
                vista.Txt_id.setText("" + id);
                vista.Txt_cedula.setText("" + cedula);
                vista.Txt_nombre.setText(nombre);
                vista.Txt_apellido.setText(apellido);
                vista.Txt_telefono.setText(telefono);
                vista.Txt_direccion.setText(direccion);
                vista.Txt_email.setText(email);
            }
        }
        if (e.getSource() == vista.btnActualizar) {
            Actualizar();
            listar(vista.tabla);
            bloquear();
            nuevo();

        }
        if (e.getSource() == vista.btnDelete) {
            delete();
            listar(vista.tabla);
            nuevo();
            bloquear();
        }
        if (e.getSource() == vista.btnNuevo) {
            desbloquear1();
            nuevo();
        }
        if (e.getSource() == vista.BtnExit) {
            vista.setVisible(false);
            //admin.setVisible(true);
        }
    }

    void nuevo() {
        vista.Txt_id.setText("");
        vista.Txt_cedula.setText("");
        vista.Txt_nombre.setText("");
        vista.Txt_apellido.setText("");
        vista.Txt_telefono.setText("");
        vista.Txt_direccion.setText("");
        vista.Txt_email.setText("");
        vista.Txt_cedula.requestFocus();
    }

    public void delete() {
        int fila = vista.tabla.getSelectedRow();
        if (fila == -1) {
            JOptionPane.showMessageDialog(vista, "Debe Seleccionar una Fila...!!!");
        } else {
            int id = Integer.parseInt((String) vista.tabla.getValueAt(fila, 0).toString());
            dao.Delete(id);
            System.out.println("El Reusltaod es" + id);
            JOptionPane.showMessageDialog(vista, "Usuario Eliminado...!!!");
        }
        limpiarTabla();
    }

    public void add() {
        int id;
        int cedula = Integer.parseInt(vista.Txt_cedula.getText());
        String nombre = vista.Txt_nombre.getText();
        String apellido = vista.Txt_apellido.getText();
        String telefono = vista.Txt_telefono.getText();
        String direccion = vista.Txt_direccion.getText();
        String email = vista.Txt_email.getText();
        p.setId(id = 0);
        p.setCedula(cedula);
        p.setNombre(nombre);
        p.setApellido(apellido);
        p.setTelefono(telefono);
        p.setDireccion(direccion);
        p.setEmail(email);
        int r = dao.agregar(p);
        if (r == 1) {
            JOptionPane.showMessageDialog(vista, "Usuario Agregado con Exito.");
        } else {
            JOptionPane.showMessageDialog(vista, "Error");
        }
        limpiarTabla();
    }

    public void Actualizar() {
        if (vista.Txt_id.getText().equals("")) {
            JOptionPane.showMessageDialog(vista, "No se Identifica el Id debe selecionar la opcion Editar");
        } else {
            int cedula = Integer.parseInt(vista.Txt_cedula.getText());
            String nombre = vista.Txt_nombre.getText();
            String apellido = vista.Txt_apellido.getText();
            String telefono = vista.Txt_telefono.getText();
            String direccion = vista.Txt_direccion.getText();
            String email = vista.Txt_email.getText();
            int id = Integer.parseInt(vista.Txt_id.getText());
            p.setCedula(cedula);
            p.setNombre(nombre);
            p.setApellido(apellido);
            p.setTelefono(telefono);
            p.setDireccion(direccion);
            p.setEmail(email);
            p.setId(id);
            int r = dao.Actualizar(p);
            if (r == 1) {
                JOptionPane.showMessageDialog(vista, "Usuario Actualizado con Exito.");
            } else {
                JOptionPane.showMessageDialog(vista, "Error");
            }
        }
        limpiarTabla();
    }

    public void listar(JTable tabla) {
        centrarCeldas(tabla);
        modelo = (DefaultTableModel) tabla.getModel();
        tabla.setModel(modelo);
        List<Cliente> lista = dao.listar();
        Object[] objeto = new Object[7];
        for (int i = 0; i < lista.size(); i++) {
            objeto[0] = lista.get(i).getId();
            objeto[1] = lista.get(i).getCedula();
            objeto[2] = lista.get(i).getNombre();
            objeto[3] = lista.get(i).getApellido();
            objeto[4] = lista.get(i).getTelefono();
            objeto[5] = lista.get(i).getDireccion();
            objeto[6] = lista.get(i).getEmail();
            modelo.addRow(objeto);
        }
        tabla.setRowHeight(35);
        tabla.setRowMargin(10);

    }

    public void Buscar(String valorl, String filtrol, JTable tablalectura) {
        String[] titulos = {"ID", "CEDULA", "NOMBRE", "APELLIDO", "TELEFONO", "DIRECCION", "EMAIL"};
        String[] registros = new String[7];
        modelo = new DefaultTableModel(null, titulos);
        String SSQL = null;
        Connection conect = null;
        if (filtrol.equals("Cedula")) {
            SSQL = "SELECT * "
                    + "FROM cliente "
                    + "WHERE cedula LIKE '%" + valorl + "%' and cliente.estado = 1;";
        } else if (filtrol.equals("Nombre")) {
            SSQL = "SELECT * "
                    + "FROM cliente "
                    + "WHERE nombre LIKE '%" + valorl + "%' and cliente.estado = 1;";
        } else if (filtrol.equals("Apellido")) {
            SSQL = "SELECT * "
                    + "FROM cliente "
                    + "WHERE apellido LIKE '%" + valorl + "%' and cliente.estado = 1;";
        } else if (filtrol.equals("Telefono")) {
            SSQL = "SELECT * "
                    + "FROM cliente "
                    + "WHERE telefono LIKE '%" + valorl + "%' and cliente.estado = 1;";
        } else if (filtrol.equals("Direccion")) {
            SSQL = "SELECT * "
                    + "FROM cliente "
                    + "WHERE direccion LIKE '%" + valorl + "%' and cliente.estado = 1;";
        } else if (filtrol.equals("Email")) {
            SSQL = "SELECT * "
                    + "FROM cliente "
                    + "WHERE email LIKE '%" + valorl + "%' and cliente.estado = 1;";
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione algun Filtro", "Error", JOptionPane.ERROR_MESSAGE);
        }
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(SSQL);
            while (rs.next()) {
                registros[0] = rs.getString("id");
                registros[1] = rs.getString("cedula");
                registros[2] = rs.getString("nombre");
                registros[3] = rs.getString("apellido");
                registros[4] = rs.getString("telefono");
                registros[5] = rs.getString("direccion");
                registros[6] = rs.getString("email");
                modelo.addRow(registros);
            }
            tablalectura.setModel(modelo);
            vista.btnListar.setEnabled(false);
            vista.Btn_reset.setEnabled(true);
        } catch (SQLException e) {
            //JOptionPane.showMessageDialog(null, e, "Error durante el procedimiento", JOptionPane.ERROR_MESSAGE);
            System.out.print(e);
        }
    }

    void centrarCeldas(JTable tabla) {
        DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();
        tcr.setHorizontalAlignment(SwingConstants.CENTER);
        for (int i = 0; i < vista.tabla.getColumnCount(); i++) {
            tabla.getColumnModel().getColumn(i).setCellRenderer(tcr);
        }
    }

    void limpiarTabla() {
        for (int i = 0; i < vista.tabla.getRowCount(); i++) {
            modelo.removeRow(i);
            i = i - 1;
        }
    }
}
