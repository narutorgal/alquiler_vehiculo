
package com.alquiler.modelo;

import java.util.Objects;

public class Sublista {

    int id;
    String id_lista;
    String sublista;
    String estado;

    public Sublista() {

    }

    public Sublista(int id, String id_lista, String sublista) {
        this.id = id;
        this.id_lista = id_lista;
        this.sublista = sublista;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getId_lista() {
        return id_lista;
    }

    public void setId_lista(String id_lista) {
        this.id_lista = id_lista;
    }

    public String getSublista() {
        return sublista;
    }

    public void setSublista(String sublista) {
        this.sublista = sublista;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    public Sublista data(){
        Sublista data = new Sublista(getId(),getId_lista(),getSublista());
        return data;
    }

    @Override
    public String toString() {
        return getSublista();
    }
    
    
}
