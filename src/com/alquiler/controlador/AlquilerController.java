package com.alquiler.controlador;

import com.alquiler.modelo.Alquiler;
import com.alquiler.modelo.AlquilerDao;
import com.alquiler.modelo.Cliente;
import com.alquiler.modelo.ClienteDao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import com.alquiler.modelo.Conexion;
import com.alquiler.modelo.Sublista;
import com.alquiler.modelo.SublistaDao;
import com.alquiler.modelo.Vehiculo;
import com.alquiler.modelo.VehiculoDao;
import com.alquiler.vista.vistaalquiler;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Vector;

public class AlquilerController implements ActionListener {

    PreparedStatement ps;
    Conexion cc = new Conexion();
    Connection cn = cc.getConnection();
    AlquilerDao dao = new AlquilerDao();
    VehiculoDao daov = new VehiculoDao();
    ClienteDao daoc = new ClienteDao();
    Alquiler p = new Alquiler();
    Vehiculo v = new Vehiculo();
    Cliente c = new Cliente();
    vistaalquiler vista = new vistaalquiler();
    DefaultTableModel modelo = new DefaultTableModel();
    Sublista sublista = new Sublista();

    public AlquilerController(vistaalquiler v) {
        this.vista = v;
        this.vista.btnListar.addActionListener(this);
        this.vista.btnAgregar.addActionListener(this);
        this.vista.btnEditar.addActionListener(this);
        this.vista.btnDelete.addActionListener(this);
        this.vista.btnActualizar.addActionListener(this);
        this.vista.btnNuevo.addActionListener(this);
        this.vista.Btn_reset.addActionListener(this);
        this.vista.Btn_buscar.addActionListener(this);
        this.vista.Btn_devolver.addActionListener(this);
        bloquear();
        Mostrarauto();
        Mostrarcliente();
    }

    public void Mostrarauto() {
        Vector<Vehiculo> oListaObjetoParaComboBox = daov.getVehiculo();
        if (oListaObjetoParaComboBox.size() > 0) {
            for (int i = 0; i < oListaObjetoParaComboBox.size(); i = i + 1) {
                Vehiculo contar = oListaObjetoParaComboBox.get(i);
                Vehiculo oItem = new Vehiculo(contar.getId(), contar.getPlaca(), contar.getMarca(), contar.getLinea());
                vista.List_auto.addItem(oItem);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No hay marcas registrados en la Base de datos", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void Mostrarcliente() {
        Vector<Cliente> oListaObjetoParaComboBox = daoc.getCliente();
        if (oListaObjetoParaComboBox.size() > 0) {
            for (int i = 0; i < oListaObjetoParaComboBox.size(); i = i + 1) {
                Cliente contar = oListaObjetoParaComboBox.get(i);
                Cliente oItem = new Cliente(contar.getId(), contar.getCedula(), contar.getNombre(), contar.getApellido(), contar.getTelefono(), contar.getDireccion(), contar.getEmail());
                vista.List_cliente.addItem(oItem);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No hay marcas registrados en la Base de datos", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void getSelect(String auto, String cedula) {
        if (auto != null) {
            Vector<Vehiculo> oListaObjetoParaComboBox = daov.getVehiculo1(auto);
            if (oListaObjetoParaComboBox.size() > 0) {
                for (int i = 0; i < oListaObjetoParaComboBox.size(); i++) {
                    Vehiculo contar = oListaObjetoParaComboBox.get(i);
                    Vehiculo oItem1 = new Vehiculo(contar.getId(), contar.getPlaca(), contar.getMarca(), contar.getLinea());
                    vista.List_auto.removeAllItems();
                    vista.List_auto.addItem(oItem1);
                    vista.List_auto.setSelectedIndex(i++);
                    Mostrarauto();
                }
            } else {
                JOptionPane.showMessageDialog(null, "No hay vehiculos registrados en la Base de datos", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            System.out.println("no envia nada");
        }
        if (cedula != null) {
            Vector<Cliente> oListaObjetoParaComboBox = daoc.getCliente1(cedula);
            if (oListaObjetoParaComboBox.size() > 0) {
                for (int i = 0; i < oListaObjetoParaComboBox.size(); i++) {
                    Cliente contar = oListaObjetoParaComboBox.get(i);
                    Cliente oItem = new Cliente(contar.getId(), contar.getCedula(), contar.getNombre(), contar.getApellido(), contar.getTelefono(), contar.getDireccion(), contar.getEmail());
                    vista.List_cliente.removeAllItems();
                    vista.List_cliente.addItem(oItem);
                    vista.List_cliente.setSelectedIndex(i);
                    Mostrarcliente();
                }
            } else {
                JOptionPane.showMessageDialog(null, "No hay clientes registrados en la Base de datos", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            System.out.println("no envia nada");
        }
    }

    void bloquear() {
        vista.List_auto.setEnabled(false);
        vista.List_cliente.setEnabled(false);
        vista.Txt_dias.setEnabled(false);
        vista.btnListar.setEnabled(true);
        vista.btnNuevo.setEnabled(true);
        vista.Btn_reset.setEnabled(false);
        vista.btnEditar.setEnabled(false);
        vista.btnDelete.setEnabled(false);
        vista.btnActualizar.setEnabled(false);
        vista.btnAgregar.setEnabled(false);
        vista.Btn_devolver.setEnabled(true);
    }
    void bloquear1() {
        vista.List_auto.setEnabled(false);
        vista.List_cliente.setEnabled(false);
        vista.Txt_dias.setEnabled(false);
        vista.btnListar.setEnabled(true);
        vista.btnNuevo.setEnabled(true);
        vista.Btn_reset.setEnabled(false);
        vista.btnEditar.setEnabled(true);
        vista.btnDelete.setEnabled(true);
        vista.btnActualizar.setEnabled(false);
        vista.btnAgregar.setEnabled(false);
        vista.Btn_devolver.setEnabled(true);
        vista.btnListar.setEnabled(false);
    }

    void desbloquear1() {
        vista.btnListar.setEnabled(true);
        vista.btnNuevo.setEnabled(false);
        vista.Btn_reset.setEnabled(false);
        vista.btnEditar.setEnabled(true);
        vista.btnDelete.setEnabled(false);
        vista.btnActualizar.setEnabled(false);
        vista.btnAgregar.setEnabled(true);
    }

    void desbloquear2() {
        vista.btnListar.setEnabled(false);
        vista.btnNuevo.setEnabled(false);
        vista.Btn_reset.setEnabled(false);
        vista.btnEditar.setEnabled(true);
        vista.btnDelete.setEnabled(true);
        vista.btnActualizar.setEnabled(false);
        vista.btnAgregar.setEnabled(true);
    }

    void desbloquear3() {
        vista.btnListar.setEnabled(false);
        vista.btnNuevo.setEnabled(false);
        vista.Btn_reset.setEnabled(false);
        vista.btnEditar.setEnabled(true);
        vista.btnDelete.setEnabled(false);
        vista.btnActualizar.setEnabled(true);
        vista.btnAgregar.setEnabled(false);
        vista.Btn_devolver.setEnabled(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == vista.Btn_buscar) {
            Buscar(vista.Txt_buscar.getText(), vista.List_buscar.getSelectedItem().toString(), vista.TablaAlquiler);
        }
        if (e.getSource() == vista.Btn_reset) {
            limpiarTabla();
            vista.List_buscar.setSelectedIndex(0);
            vista.Txt_buscar.setText("");
            vista.btnListar.setEnabled(true);
            vista.Txt_dias.requestFocus();
        }
        if (e.getSource() == vista.btnListar) {
            desbloquear2();
            limpiarTabla();
            listar(vista.TablaAlquiler);
            nuevo();
        }
        if (e.getSource() == vista.btnAgregar) {
            Cliente item = (Cliente) vista.List_cliente.getSelectedItem();
            Vehiculo item2 = (Vehiculo) vista.List_auto.getSelectedItem();
            add(item.getId(), item2.getId());
            listar(vista.TablaAlquiler);
            bloquear();
            vista.List_auto.removeAllItems();
            Vehiculo data2 = new Vehiculo(0, "Seleccione...", "0", "0");
            Cliente data = new Cliente(0,0,"Seleccione...","","","","");
            vista.List_auto.addItem(data2);
            vista.List_cliente.addItem(data);
            nuevo();
            Mostrarauto();
            Mostrarcliente();
        }
        if (e.getSource() == vista.btnEditar) {
            desbloquear3();
            int fila = vista.TablaAlquiler.getSelectedRow();
            if (fila == -1) {
                JOptionPane.showMessageDialog(vista, "Debee Seleccionar Una fila..!!");
            } else {
                String estado = (String) vista.TablaAlquiler.getValueAt(fila, 5);
                if (estado.equals("Devuelto")) {
                    JOptionPane.showMessageDialog(vista, "El Alquiler fue devuelto y no se puede modificar");
                    bloquear();
                    vista.btnEditar.setEnabled(true);
                    nuevo();
                } else {
                    int id = Integer.parseInt((String) vista.TablaAlquiler.getValueAt(fila, 0).toString());
                    String cliente = vista.TablaAlquiler.getValueAt(fila, 1).toString();
                    String[] parts = cliente.split("-");
                    String identificacion = parts[0];
                    String vehiculo = vista.TablaAlquiler.getValueAt(fila, 2).toString();
                    int dias = Integer.parseInt((String) vista.TablaAlquiler.getValueAt(fila, 3).toString());
                    vista.Txt_id.setText("" + id);
                    vista.Txt_dias.setText("" + dias);
                    getSelect(vehiculo, identificacion);
                    vista.List_auto.setEnabled(false);
                    vista.List_cliente.setEnabled(false);
                    
                }

            }
        }
        if (e.getSource() == vista.btnActualizar) {
            Cliente item = (Cliente) vista.List_cliente.getSelectedItem();
            Vehiculo item2 = (Vehiculo) vista.List_auto.getSelectedItem();
            Actualizar(item.getId(), item2.getId());
            listar(vista.TablaAlquiler);
            bloquear1();
            vista.List_auto.removeAllItems();
            vista.List_cliente.removeAllItems();
            Vehiculo data2 = new Vehiculo(0, "Seleccione...", "0", "0");
            Cliente data = new Cliente(0,0,"Seleccione...","","","","");
            vista.List_auto.addItem(data2);
            vista.List_cliente.addItem(data);
            Mostrarauto();
            Mostrarcliente();
            limpiar();

        }
        if (e.getSource() == vista.btnDelete) {
            delete();
            limpiarTabla();
            nuevo();
            bloquear();
        }
        if (e.getSource() == vista.btnNuevo) {
            desbloquear1();
            nuevo();
        }
        if (e.getSource() == vista.Btn_devolver) {
            devolver();
            desbloquear1();
            nuevo();
        }
    }

    void nuevo() {
        vista.List_auto.setEnabled(true);
        vista.List_cliente.setEnabled(true);
        vista.Txt_dias.setEnabled(true);
        vista.Txt_id.setText("");
        vista.Txt_dias.setText("");
        vista.List_auto.setSelectedIndex(0);
        vista.List_cliente.setSelectedIndex(0);
        vista.Txt_dias.requestFocus();
    }
    void limpiar(){
        vista.Txt_id.setText("");
        vista.Txt_dias.setText("");
        vista.List_auto.setSelectedIndex(0);
        vista.List_cliente.setSelectedIndex(0);
    }

    public void Buscar(String valorl, String filtrol, JTable tablalectura) {
        String[] titulos = {"ID", "NOMBRE CLIENTE", "PLACA AUTO", "DIAS", "FECHA", "ESTADO"};
        String[] registros = new String[6];
        modelo = new DefaultTableModel(null, titulos);
        String SSQL = null;
        Connection conect = null;
        if (filtrol.equals("Nombre Cliente")) {
            SSQL = "SELECT alquiler.id, CONCAT(cliente.cedula,'-',cliente.nombre,' ',cliente.apellido) as nombre_cliente , auto.placa, cant_dias, fecha, "
                    + "CASE WHEN alquiler.estado= 1 THEN 'En Curso' "
                    + "WHEN alquiler.estado= 2 THEN 'Devuelto' "
                    + "WHEN alquiler.estado=3 THEN 'Demorado' "
                    + "Else 'Eliminado' END as estado_alquiler "
                    + "FROM alquiler "
                    + "LEFT JOIN cliente ON alquiler.id_cliente = cliente.id "
                    + "LEFT JOIN auto ON alquiler.id_auto = auto.id "
                    + "WHERE cliente.nombre LIKE '%" + valorl + "%';";
        } else if (filtrol.equals("Apellido Cliente")) {
            SSQL = "SELECT alquiler.id, CONCAT(cliente.cedula,'-',cliente.nombre,' ',cliente.apellido) as nombre_cliente , auto.placa, cant_dias, fecha, "
                    + "CASE WHEN alquiler.estado= 1 THEN 'En Curso' "
                    + "WHEN alquiler.estado= 2 THEN 'Devuelto' "
                    + "WHEN alquiler.estado=3 THEN 'Demorado' "
                    + "Else 'Eliminado' END as estado_alquiler "
                    + "FROM alquiler "
                    + "LEFT JOIN cliente ON alquiler.id_cliente = cliente.id "
                    + "LEFT JOIN auto ON alquiler.id_auto = auto.id "
                    + "WHERE cliente.apellido LIKE '%" + valorl + "%';";
        } else if (filtrol.equals("Placa")) {
            SSQL = "SELECT alquiler.id, CONCAT(cliente.cedula,'-',cliente.nombre,' ',cliente.apellido) as nombre_cliente , auto.placa, cant_dias, fecha, "
                    + "CASE WHEN alquiler.estado= 1 THEN 'En Curso' "
                    + "WHEN alquiler.estado= 2 THEN 'Devuelto' "
                    + "WHEN alquiler.estado=3 THEN 'Demorado' "
                    + "Else 'Eliminado' END as estado_alquiler "
                    + "FROM alquiler "
                    + "LEFT JOIN cliente ON alquiler.id_cliente = cliente.id "
                    + "LEFT JOIN auto ON alquiler.id_auto = auto.id "
                    + "WHERE auto.placa LIKE '%" + valorl + "%';";
        } else if (filtrol.equals("Dias")) {
            SSQL = "SELECT alquiler.id, CONCAT(cliente.cedula,'-',cliente.nombre,' ',cliente.apellido) as nombre_cliente , auto.placa, cant_dias, fecha, "
                    + "CASE WHEN alquiler.estado= 1 THEN 'En Curso' "
                    + "WHEN alquiler.estado= 2 THEN 'Devuelto' "
                    + "WHEN alquiler.estado= 3 THEN 'Demorado' "
                    + "Else 'Eliminado' END as estado_alquiler "
                    + "FROM alquiler "
                    + "LEFT JOIN cliente ON alquiler.id_cliente = cliente.id "
                    + "LEFT JOIN auto ON alquiler.id_auto = auto.id "
                    + "WHERE alquiler.cant_dias = " + valorl + " and auto.estado = 1;";
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione algun Filtro", "Error", JOptionPane.ERROR_MESSAGE);
        }
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(SSQL);
            while (rs.next()) {
                registros[0] = rs.getString("alquiler.id");
                registros[1] = rs.getString("nombre_cliente");
                registros[2] = rs.getString("auto.placa");
                registros[3] = rs.getString("cant_dias");
                registros[4] = rs.getString("fecha");
                registros[5] = rs.getString("estado_alquiler");
                modelo.addRow(registros);
            }
            tablalectura.setModel(modelo);
            vista.btnListar.setEnabled(false);
            vista.Btn_reset.setEnabled(true);
        } catch (SQLException e) {
            //JOptionPane.showMessageDialog(null, e, "Error durante el procedimiento", JOptionPane.ERROR_MESSAGE);
            System.out.print(e);
        }
    }

    public void delete() {
        int fila = vista.TablaAlquiler.getSelectedRow();
        if (fila == -1) {
            JOptionPane.showMessageDialog(vista, "Debe Seleccionar una Fila...!!!");
        } else {
            int id = Integer.parseInt((String) vista.TablaAlquiler.getValueAt(fila, 0).toString());
            dao.Delete(id);
            System.out.println("El Resultado es" + id);
            JOptionPane.showMessageDialog(vista, "Usuario Eliminado...!!!");
        }
        limpiarTabla();
    }

    public void devolver() {
        int fila = vista.TablaAlquiler.getSelectedRow();
        if (fila == -1) {
            JOptionPane.showMessageDialog(vista, "Debe Seleccionar una Fila...!!!");
        } else {
            int id = Integer.parseInt((String) vista.TablaAlquiler.getValueAt(fila, 0).toString());
            String vehiculo = vista.TablaAlquiler.getValueAt(fila, 2).toString();
            dao.Devolucion(id);
            dao.Disponible(vehiculo);
            JOptionPane.showMessageDialog(vista, "Vehiculo Regresado...!!!");
        }
        limpiarTabla();
        nuevo();
    }

    public void add(int cliente, int auto) {
        int dias = Integer.parseInt(vista.Txt_dias.getText());
        String cliente1 = String.valueOf(cliente);
        String auto1 = String.valueOf(auto);
        p.setId_cliente(cliente1);
        p.setId_auto(auto1);
        p.setCant_dias(dias);
        int r = dao.agregar(p);
        daov.Estado(auto);
        if (r == 1) {
            JOptionPane.showMessageDialog(vista, "Alquiler Registrado con Exito.");
        } else {
            JOptionPane.showMessageDialog(vista, "Error");
        }
        limpiarTabla();
    }

    public void Actualizar(int cliente, int auto) {
        if (vista.Txt_id.getText().equals("")) {
            JOptionPane.showMessageDialog(vista, "No se Identifica el Id debe selecionar la opcion Editar");
        } else {
            int dias = Integer.parseInt(vista.Txt_dias.getText());
            int id = Integer.parseInt(vista.Txt_id.getText());
            String cliente1 = String.valueOf(cliente);
            String auto1 = String.valueOf(auto);
            p.setCant_dias(dias);
            p.setId_cliente(cliente1);
            p.setId_auto(auto1);
            p.setId(id);
            int r = dao.Actualizar(p);
            if (r == 1) {
                JOptionPane.showMessageDialog(vista, "Alquiler Actualizado con Exito.");
            } else {
                JOptionPane.showMessageDialog(vista, "Error");
            }
        }
        limpiarTabla();
    }

    public void Estado(String cliente, String auto) {
        if (vista.Txt_id.getText().equals("")) {
            JOptionPane.showMessageDialog(vista, "No se Identifica el Id debe selecionar la opcion Editar");
        } else {
            int dias = Integer.parseInt(vista.Txt_dias.getText());
            int id = Integer.parseInt(vista.Txt_id.getText());
            p.setCant_dias(dias);
            p.setId_cliente(cliente);
            p.setId_auto(auto);
            p.setId(id);
            int r = dao.Actualizar(p);
            if (r == 1) {
                JOptionPane.showMessageDialog(vista, "Vehiculo Actualizado con Exito.");
            } else {
                JOptionPane.showMessageDialog(vista, "Error");
            }
        }
        limpiarTabla();
    }
    public void listar(JTable tabla) {
        centrarCeldas(tabla);
        modelo = (DefaultTableModel) tabla.getModel();
        tabla.setModel(modelo);
        List<Alquiler> lista = dao.listar();
        Object[] objeto = new Object[6];
        for (int i = 0; i < lista.size(); i++) {
            objeto[0] = lista.get(i).getId();
            objeto[1] = lista.get(i).getId_cliente();
            objeto[2] = lista.get(i).getId_auto();
            objeto[3] = lista.get(i).getCant_dias();
            objeto[4] = lista.get(i).getFecha();
            objeto[5] = lista.get(i).getEstado();
            modelo.addRow(objeto);
        }
        tabla.setRowHeight(35);
        tabla.setRowMargin(10);
    }

    void centrarCeldas(JTable tabla) {
        DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();
        tcr.setHorizontalAlignment(SwingConstants.CENTER);
        for (int i = 0; i < vista.TablaAlquiler.getColumnCount(); i++) {
            tabla.getColumnModel().getColumn(i).setCellRenderer(tcr);
        }
    }

    public List<String[]> Dias() {
        List<String[]> tipoAlquiler = new ArrayList<>();
        String sSQL = null;
        sSQL = "SELECT fecha,cant_dias,id,estado from alquiler where estado = 1";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                String[] registros = new String[4];
                registros[0] = rs.getString("fecha");
                registros[1] = rs.getString("cant_dias");
                registros[2] = rs.getString("id");
                registros[3] = rs.getString("estado");
                tipoAlquiler.add(registros);
            }
            return tipoAlquiler;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
    }

    public void limpiarTabla() {
        for (int i = 0; i < vista.TablaAlquiler.getRowCount(); i++) {
            modelo.removeRow(i);
            i = i - 1;
        }
    }

    public void vencimiento(int id_alquiler) {
        int id = id_alquiler;
        dao.Vencimiento(id);
        System.out.println("El Reusltado es " + id);
        limpiarTabla();
    }

}
