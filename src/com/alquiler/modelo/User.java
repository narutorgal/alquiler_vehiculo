package com.alquiler.modelo;

public class User {
    int id_usuario;
    String username;
    String password;
    int id_tipousuario;
    String estado;
    
    public User(){
        
    }

    public User(String username, String password, int id_tipousuario) {
        this.username = username;
        this.password = password;
        this.id_tipousuario = id_tipousuario;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId_tipousuario() {
        return id_tipousuario;
    }

    public void setId_tipousuario(int id_tipousuario) {
        this.id_tipousuario = id_tipousuario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
}

