package com.alquiler.modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class Conexion {

    Connection conectar=null;
    
    public Connection getConnection(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conectar=DriverManager.getConnection("jdbc:mysql://localhost/vehiculo_alquiler","root","");
            } catch (ClassNotFoundException | SQLException e) {
            JOptionPane.showMessageDialog(null, "FALLO LA CONEXION A LA BASE DE DATOS, COMUNIQUESE CON EL ADMINISTRADOR");
            System.exit(0);
        }
        return conectar;
    }
}
