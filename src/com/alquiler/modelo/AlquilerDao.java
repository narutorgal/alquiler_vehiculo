
package com.alquiler.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class AlquilerDao {

    PreparedStatement ps;
    ResultSet rs;
    Connection con;
    Conexion conectar = new Conexion();
    Alquiler p = new Alquiler();

    public List listar() {
        List<Alquiler> datos = new ArrayList<>();
        String sql = "SELECT alquiler.id, CONCAT(cliente.cedula,'-',cliente.nombre,' ',cliente.apellido) as nombre_cliente, auto.placa, cant_dias, fecha,"
                    + "CASE WHEN alquiler.estado= 1 THEN 'En Curso' "
                    + "WHEN alquiler.estado= 2 THEN 'Devuelto' "
                    + "WHEN alquiler.estado=3 THEN 'Demorado' "
                    + "Else 'Eliminado' END as estado_alquiler "
                    + "FROM alquiler "
                    + "LEFT JOIN cliente ON alquiler.id_cliente = cliente.id "
                    + "LEFT JOIN auto ON alquiler.id_auto = auto.id "
                    + "where alquiler.estado != 0 ";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Alquiler p = new Alquiler();
                p.setId(rs.getInt(1));
                p.setId_cliente(rs.getString(2));
                p.setId_auto(rs.getString(3));
                p.setCant_dias(rs.getInt(4));
                p.setFecha(rs.getString(5));
                p.setEstado(rs.getString(6));
                datos.add(p);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e+" - "+sql, "Error", JOptionPane.ERROR_MESSAGE);
            System.out.print(e);
            System.out.print(sql);
        }
        return datos;
    }

    public int agregar(Alquiler per) {
        int r = 0;
        String sql = "INSERT INTO alquiler (id_cliente, id_auto, cant_dias) VALUES (?,?,?);";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, per.getId_cliente());
            ps.setString(2, per.getId_auto());
            ps.setInt(3, per.getCant_dias());
            r = ps.executeUpdate();
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e+" - "+sql, "Error", JOptionPane.ERROR_MESSAGE);
            System.out.print(e);
            System.out.print(sql);
        }
        return r;
    }

    public int Actualizar(Alquiler per) {
        int r = 0;
        String sql = "update alquiler set id_cliente=?,id_auto=?,cant_dias=?, estado=1 where id=?";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, per.getId_cliente());
            ps.setString(2, per.getId_auto());
            ps.setInt(3, per.getCant_dias());
            ps.setInt(4, per.getId());
            r = ps.executeUpdate();
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e+" - "+sql, "Error", JOptionPane.ERROR_MESSAGE);
            System.out.print(e);
            System.out.print(sql);
        }
        return r;
    }

    public int Delete(int id) {
        int r = 0;
        String sql = "update alquiler set estado = 0 where Id=" + id;
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            r = ps.executeUpdate();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e+" - "+sql, "Error", JOptionPane.ERROR_MESSAGE);
            System.out.print(e);
            System.out.print(sql);
        }
        return r;
    }

    public int Devolucion(int id) {
        int r = 0;
        String sql = "update alquiler set estado = 2 where Id=" + id;
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            r = ps.executeUpdate();
            //JOptionPane.showMessageDialog(null, sql);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e+" - "+sql, "Error", JOptionPane.ERROR_MESSAGE);
            System.out.print(e);
            System.out.print(sql);
        }
        return r;
    }

    public int Vencimiento(int id) {
        int r = 0;
        String sql = "update alquiler set estado = 3 where Id=" + id;
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            r = ps.executeUpdate();
            JOptionPane.showMessageDialog(null, sql);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e+" - "+sql, "Error", JOptionPane.ERROR_MESSAGE);
            System.out.print(e);
            System.out.print(sql);
        }
        return r;
    }

    public int Estado(int id) {
        int r = 0;
        String sql = "update auto set estado = 2 where Id=" + id;
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            r = ps.executeUpdate();
            JOptionPane.showMessageDialog(null, sql);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e+" - "+sql, "Error", JOptionPane.ERROR_MESSAGE);
            System.out.print(e);
            System.out.print(sql);
        }
        return r;
    }

    public int Disponible(String id) {
        int r = 0;
        String sql = "update auto set estado = 1 where placa = '" + id + "';";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            r = ps.executeUpdate();
            JOptionPane.showMessageDialog(null, sql);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e+" - "+sql, "Error", JOptionPane.ERROR_MESSAGE);
            System.out.print(e);
            System.out.print(sql);
        }
        return r;
    }
}
