package com.alquiler.modelo;


public class Listas {
    int id;
    String lista;
    String estado;
    
    public Listas(){
        
    }

    public Listas(int id, String lista) {
        this.id = id;
        this.lista = lista;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLista() {
        return lista;
    }

    public void setLista(String lista) {
        this.lista = lista;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return getLista();
    }
}
