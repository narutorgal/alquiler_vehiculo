
package com.alquiler.modelo;

public class Vehiculo {

    int id;
    String placa;
    String marca;
    String linea;
    String estado;

    public Vehiculo() {

    }

    public Vehiculo(int id, String placa, String marca, String linea) {
        this.id = id;
        this.placa = placa;
        this.marca = marca;
        this.linea = linea;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return getPlaca();
    }

}
