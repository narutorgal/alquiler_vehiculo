package com.alquiler.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class UserDao {

    PreparedStatement ps;
    ResultSet rs;
    Connection con;
    Conexion conectar = new Conexion();
    User p = new User();

    public List listar() {
        List<User> datos = new ArrayList<>();
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement("select * from usuario where estado != 0");
            rs = ps.executeQuery();
            while (rs.next()) {
                User p = new User();
                p.setId_usuario(rs.getInt(1));
                p.setUsername(rs.getString(2));
                p.setPassword(rs.getString(3));
                p.setId_tipousuario(rs.getInt(4));
                p.setEstado(rs.getString(5));
                datos.add(p);
            }
        } catch (Exception e) {
        }
        return datos;
    }

    public int agregar(User per) {
        int r = 0;
        String sql = "insert into usuario(username,password,id_tipousuario)values(?,?,?)";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, per.getUsername());
            ps.setString(2, per.getPassword());
            ps.setInt(3, per.getId_tipousuario());
            r = ps.executeUpdate();
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
        }
        return r;
    }

    public int Actualizar(User per) {
        int r = 0;
        String sql = "update usuario set username=?,password=?,id_tipousuario=? where id_usuario=?";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, per.getUsername());
            ps.setString(2, per.getPassword());
            ps.setInt(3, per.getId_tipousuario());
            ps.setInt(4, per.getId_usuario());
            r = ps.executeUpdate();
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.print(e);
        }
        return r;
    }

    public int Delete(int id) {
        int r = 0;
        String sql = "update usuario set estado = 0 where id_usuario=" + id;
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            r = ps.executeUpdate();
        } catch (Exception e) {
        }
        return r;
    }
}
