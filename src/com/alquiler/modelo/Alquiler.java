package com.alquiler.modelo;

public class Alquiler {
    int id;
    String id_cliente;
    String id_auto;
    String fecha;
    int cant_dias;
    String estado;
    
    public Alquiler(){
        
    }

    public Alquiler(String id_cliente, String id_auto, int cant_dias) {
        this.id_cliente = id_cliente;
        this.id_auto = id_auto;
        this.cant_dias = cant_dias;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(String id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getId_auto() {
        return id_auto;
    }

    public void setId_auto(String id_auto) {
        this.id_auto = id_auto;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getCant_dias() {
        return cant_dias;
    }

    public void setCant_dias(int cant_dias) {
        this.cant_dias = cant_dias;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    
}
