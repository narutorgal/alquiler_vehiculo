-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-03-2022 a las 09:56:55
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vehiculo_alquiler`
--
CREATE DATABASE IF NOT EXISTS `vehiculo_alquiler` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `vehiculo_alquiler`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alquiler`
--

CREATE TABLE `alquiler` (
  `id` int(20) NOT NULL,
  `id_cliente` int(10) NOT NULL,
  `id_auto` int(20) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp(),
  `cant_dias` int(20) NOT NULL,
  `estado` varchar(1) NOT NULL DEFAULT '1' COMMENT '0- Eliminado\r\n1- En Curso\r\n2- Devuelto\r\n3- Demorado'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auto`
--

CREATE TABLE `auto` (
  `id` int(20) NOT NULL,
  `placa` varchar(10) NOT NULL,
  `linea` int(20) NOT NULL,
  `marca` int(20) NOT NULL,
  `estado` varchar(1) NOT NULL DEFAULT '1' COMMENT '0- Eliminado \r\n1- Disponible\r\n2- Alquilado'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id` int(10) NOT NULL,
  `cedula` int(15) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `telefono` varchar(255) NOT NULL,
  `direccion` varchar(10) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `estado` varchar(1) NOT NULL DEFAULT '1' COMMENT '0- Eliminado\r\n1- Activo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `listas`
--

CREATE TABLE `listas` (
  `id` int(20) NOT NULL,
  `lista` varchar(255) NOT NULL,
  `estado` varchar(1) NOT NULL DEFAULT '1' COMMENT '1- Activo\r\n0- No Activo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `listas`
--

INSERT INTO `listas` (`id`, `lista`, `estado`) VALUES
(1, 'MARCA', '1'),
(2, 'LINEA', '1'),
(3, 'TIPO USUARIO', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sublista`
--

CREATE TABLE `sublista` (
  `id` int(20) NOT NULL,
  `id_lista` int(20) NOT NULL,
  `sublista` varchar(255) NOT NULL,
  `estado` varchar(1) NOT NULL DEFAULT '1' COMMENT '0- No Activo\r\n1- Activo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `sublista`
--

INSERT INTO `sublista` (`id`, `id_lista`, `sublista`, `estado`) VALUES
(1, 2, 'BUSETA EJECUTIVA', '1'),
(2, 2, 'MOTO', '1'),
(3, 2, 'CAMIONETA EJECUTIVA', '1'),
(4, 2, 'AUTOMOVIL', '1'),
(5, 2, 'CAMIONETA', '1'),
(6, 2, 'BUSETA', '1'),
(7, 3, 'ADMINISTRADOR', '1'),
(8, 3, 'USUARIO', '1'),
(9, 1, 'CHEVROLET', '1'),
(10, 1, 'RENAULT', '1'),
(11, 1, 'AUTECO', '1'),
(12, 1, 'TOYOTA 4*4', '1'),
(13, 1, 'NISSAN', '1'),
(14, 2, 'CUATRIMOTO', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(10) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `id_tipousuario` int(1) NOT NULL DEFAULT 1,
  `estado` varchar(1) NOT NULL DEFAULT '1' COMMENT '0- Inactivo\r\n1- Activo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `username`, `password`, `id_tipousuario`, `estado`) VALUES
(1, 'admin', 'admin', 7, '1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alquiler`
--
ALTER TABLE `alquiler`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_auto` (`id_auto`),
  ADD KEY `fk_cliente` (`id_cliente`);

--
-- Indices de la tabla `auto`
--
ALTER TABLE `auto`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `placa` (`placa`),
  ADD KEY `fk_marca` (`marca`),
  ADD KEY `fk_linea` (`linea`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cedula` (`cedula`);

--
-- Indices de la tabla `listas`
--
ALTER TABLE `listas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sublista`
--
ALTER TABLE `sublista`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_listas` (`id_lista`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`) USING BTREE,
  ADD KEY `fk_tipousuario` (`id_tipousuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alquiler`
--
ALTER TABLE `alquiler`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auto`
--
ALTER TABLE `auto`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `listas`
--
ALTER TABLE `listas`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `sublista`
--
ALTER TABLE `sublista`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `alquiler`
--
ALTER TABLE `alquiler`
  ADD CONSTRAINT `fk_auto` FOREIGN KEY (`id_auto`) REFERENCES `auto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `auto`
--
ALTER TABLE `auto`
  ADD CONSTRAINT `fk_linea` FOREIGN KEY (`linea`) REFERENCES `sublista` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_marca` FOREIGN KEY (`marca`) REFERENCES `sublista` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `sublista`
--
ALTER TABLE `sublista`
  ADD CONSTRAINT `fk_listas` FOREIGN KEY (`id_lista`) REFERENCES `listas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_tipousuario` FOREIGN KEY (`id_tipousuario`) REFERENCES `sublista` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
